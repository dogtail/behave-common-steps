# -- FILE: features/steps/example_steps.py
import logging
import os
import subprocess
from time import sleep

from behave import then, step  # pylint: disable=E0611
from dogtail.rawinput import typeText, click, pressKey, keyCombo
from dogtail.tree import root, SearchError
from gi.repository import GLib, Gdk  # pylint: disable=E0611

from . import App, wait_until

log = logging.getLogger('app')

@step(u'Press "{sequence}"')
def press_button_sequence(context, sequence):  # pylint: disable=W0613
    keyCombo(sequence)
    sleep(0.5)


def wait_for_app_to_appear(context, app, timeout):
    # Waiting for a window to appear
    for i in range(0, timeout):
        try:
            context.app.instance = root.application(context.app.a11yAppName)
            context.app.instance.child(roleName='frame')
            break
        except (GLib.GError, SearchError):
            sleep(1)
            continue
    context.execute_steps(u"Then %s should start" % app)


@step(u'Start {app} via {app_type:w}')
def start_app_via_command(context, app, app_type):
    for attempt in range(0, 10):
        try:
            if app_type == 'command':
                context.app.startViaCommand()
            if app_type == 'menu':
                context.app.startViaMenu()
            context.app.instance = root.application(context.app.a11yAppName)
            break
        except GLib.GError:
            sleep(1)
            if attempt == 6:
                # Killall the app processes if app didn't show up
                # after 5 seconds
                subprocess.call(['pkill', '-f', context.app.internCommand])
                context.app.cleanup()
                context.execute_steps(u"* Start %s via command" % app.lower())
            continue

@step(u'Start {app} via command in session')
def start_app_via_command_in_session(context, app):
    for attempt in range(0, 10):
        try:
            context.app.startViaCommand(insideSession=True)
            context.app.instance = root.application(context.app.a11yAppName)
            break
        except AttributeError:
            context.app = App(app, a11yAppName=app, recordVideo=True)
        except GLib.GError:
            sleep(1)
            if attempt == 6:
                # Killall the app processes if app didn't show up
                # after 5 seconds
                subprocess.call(['pkill', '-f', context.app.internCommand])
                context.app.cleanup()
                context.execute_steps(u"* Start %s via command in session" % app.lower())
            continue


@step(u'Close app via gnome panel')
def close_app_via_gnome_panel(context):
    context.app.closeViaGnomePanel()


@step(u'Make sure that {app} is running')
@step(u'Make sure that {app} is running waiting for {timeout} seconds')
def ensure_app_running(context, app, timeout=10):
    start_app_via_command(context, app, 'menu')
    wait_for_app_to_appear(context, app, timeout)
    log.debug("app = %s", root.application(context.app.a11yAppName))


@step(u'{app} should start')
def test_app_started(context, app):  # pylint: disable=W0613
    if app == context.app.appCommand or app == context.app.a11yAppName:
        app = context.app.a11yAppName
    log.debug('app = {}'.format(app))
    for i in range(0, 10):
        try:
            root.application(app).child(roleName='frame')
            break
        except SearchError:
            raise RuntimeError("App '%s' is not running" % app)
        except GLib.GError:
            continue


@step(u"{app} shouldn't be running anymore")
def then_app_is_dead(context, app):  # pylint: disable=W0613
    if app == context.app.appCommand or app == context.app.a11yAppName:
        app = context.app.a11yAppName
    isRunning = True
    for i in range(0, 10):
        try:
            root.application(app).child(roleName='frame')
            sleep(1)
        except SearchError:
            isRunning = False
            break
        except GLib.GError:
            continue
    if isRunning:
        raise RuntimeError("App '%s' is running" % app)


@step(u'Help section "{name}" is displayed')
def help_is_displayed(context, name):
    try:
        context.yelp = root.application('yelp')
        sleep(2)
        frame = context.yelp.child(roleName='frame')
        wait_until(lambda x: x.showing, frame)
        context.assertion.assertEquals(name, frame.name)
    finally:
        os.system("killall yelp")


def isKDEAppRunning(self):
    """
    Is the app running?
    """
    print("isKDEAppRunning: start")
    if self.a11yAppName is None:
        self.a11yAppName = self.internCommand

    # Trap weird bus errors
    for i in range(0, 30):
        try:
            print("isKDEAppRunning: Attempt #%d:" % i)
            child = root.child(name=self.a11yAppName, roleName='application', retry=False, recursive=False)
            print("isKDEAppRunning: Bingo! Got %s, exiting" % child)
            return child
        except Exception as e:
            print('isKDEAppRunning: exception "%s"' % e)
            sleep(1)
            continue
    return False


@step(u'Start {app} via KDE command')
def start_app_via_kde_command(context, app):
    context.app.startViaCommand()
    assert wait_until(lambda x: isKDEAppRunning(x), context.app, timeout=30),\
        "Application failed to start"
    context.app.instance = root.application(context.app.a11yAppName)


@step(u'Start {app} via KDE menu')
def startViaKDEMenu(context, app):
    """ Will run the app through the standard application launcher """
    corner_distance = 10
    height = Gdk.Display.get_default().get_default_screen().get_root_window().get_height()
    click(corner_distance, height - corner_distance)
    plasma = root.application('plasma-desktop')
    plasma.child(name='Search:', roleName='label').parent.child(roleName='text').text = context.app.appCommand
    sleep(0.5)
    pressKey('enter')
    assert wait_until(lambda x: isKDEAppRunning(x), context.app, timeout=30),\
        "Application failed to start"
    context.app.instance = root.application(context.app.a11yAppName)


@step(u'Launch {app} via KRunner')
def startViaKRunner(context, app):
    """ Simulates running app through Run command interface (alt-F2...)"""
    os.system('krunner')
    assert wait_until(lambda x: x.application('krunner'), root),\
        "KRunner didn't start"
    typeText(context.app.appCommand)
    sleep(1)
    pressKey('enter')
    assert wait_until(lambda x: isKDEAppRunning(x), context.app, timeout=30),\
        "Application failed to start"
    context.app.instance = root.application(context.app.a11yAppName)


@step(u'Close {app} via menu in KDE')
@step(u'Close {app} via menu "{menu}" in KDE')
@step(u'Close {app} via menu "{menu}" item "{menuitem}" in KDE')
def closeViaMenu(context, app, menu='File', menuitem='Quit', dialog=False):
    """ Does execute 'Quit' item in the main menu """
    if not wait_until(lambda x: isKDEAppRunning(x), context.app, timeout=5):
        return False
    context.app.instance.child(name=menu, roleName='menu item').click()
    sleep(1)
    context.app.instance.child(name=menuitem, roleName='menu item').click()


@step(u'Click "{menuitem}" in context menu of {app} in GnomeShell')
def clich_menu_item_in_gnomeshell(context, menuitem, app):
    """ Searches for the app in GnomeShell, right-clicks on it to get context menu and click to givem nmenuitem """
    desktopConfig = context.app.parseDesktopFile()

    gnomeShell = root.application('gnome-shell')
    os.system("dbus-send --session --type=method_call " +
              "--dest='org.gnome.Shell' " +
              "'/org/gnome/Shell' " +
              "org.gnome.Shell.FocusSearch")
    textEntry = gnomeShell.textentry('')
    assert wait_until(lambda x: x.showing, textEntry), \
        "Can't find gnome shell search textbar"

    app_name = context.app.getName(desktopConfig)
    typeText(app_name)
    sleep(1)
    icons = gnomeShell.findChildren(lambda x: x.roleName == 'label' and x.name == app_name and x.showing)
    assert wait_until(lambda x: len(x) > 0, icons), \
        "Can't find gnome shell icon for '%s'" % app_name
    icons[0].click(button=3)
    sleep(1)
    gnomeShell.findChildren(lambda x: x.name == menuitem and x.roleName == 'label' and x.showing)[0].click()
