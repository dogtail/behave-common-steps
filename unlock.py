#! /usr/bin/python3
from gi import Repository, require_version
if Repository.get_default().enumerate_versions('Secret'):
    require_version('Secret', '1')
    from gi.repository import Secret

collection = Secret.Collection.create_sync(None, "session", "default", Secret.CollectionCreateFlags.COLLECTION_CREATE_NONE, None)


