#!/usr/bin/bash

function start-gdm () {	
  systemctl stop gdm
  pgrep -lf gdm; ret=$?
  if [ ${ret} -eq 0 ]; then
    killall gdm
  fi

  grep "AutomaticLogin=test" /etc/gdm/custom.conf; ret=$?
  if [ $ret -ne 0 ]; then
    sed -i s/'\[daemon\]'/"[daemon]\nAutomaticLogin=test\nAutomaticLoginEnable=true"/ /etc/gdm/custom.conf
  fi

  starting=true
  waittime=5
  while $starting; do
    systemctl start gdm
    sleep $waittime
    if [ $(pgrep -lf gdm | wc -l) -eq 4 ]; then
      starting=false
    else
      systemctl stop gdm
      waittime=$(( $waittime + 1 ))
    fi
  done
}

function stop-gdm () {
  loginctl terminate-user test
  sudo -u test kill -9 -1
  sleep 2
  pgrep -f gdm; ret=$?
  if [ $ret -eq 0 ]; then
    systemctl stop gdm
  fi
  pgrep -f Xorg; ret=$?
  if [ $ret -eq 0 ]; then
    killall Xorg
  fi
}
