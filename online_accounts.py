# -*- coding: UTF-8 -*-
import logging
import sys
import pyatspi
from subprocess import Popen, call
from behave import step
from dogtail.predicate import GenericPredicate
from dogtail.rawinput import pressKey, keyCombo, typeText
from dogtail.tree import root
from dogtail.utils import doDelay
from common_steps import wait_until, get_showing_node_name
from gi.repository import GLib
from time import sleep

log = logging.getLogger('online_accounts')
log.setLevel(logging.DEBUG)

Google_accounts = ['Google', 'Google1', 'Google1_evo', 'Google_evo', 'Google_docs']
accounts = Google_accounts + ['Facebook', 'Microsoft Account']

@step('Open {panel} panel')
def open_panel(context, panel):
    Popen('gnome-control-center %s' % panel, shell=True)
    context.app.instance = root.application('gnome-control-center')
    pressKey('Esc')
    sleep(1)

def add_google_account(context, user, password):
    gcc = context.app.instance
    dialog = gcc.child('Google Account')
    pb = dialog.child(roleName='progress bar')
    for attempt in range (0,20):
        if pb.showing:
            sleep(0.25)
        else:
            break
    email = get_showing_node_name('__empty__', dialog, rolename='entry')
    email.text = user
    pressKey('Enter')
    for attempt in range (0,20):
        keyCombo('Tab')
        keyCombo('<Shift>Tab')
        passwd = dialog.findChild(GenericPredicate(roleName='password text'), retry=False, requireResult=False)
        if passwd:
            if passwd.showing and passwd.sensitive:
                sleep(0.25)
                break
        sleep(0.25)
    passwd.click()
    passwd.text = password
    pressKey('Enter')

    for attempts in range(0, 80):  # pylint: disable=W0612
        allow_button = dialog.findChild(GenericPredicate("ALLOW", roleName='push button'), retry=False, requireResult=False)
        if not allow_button:
            allow_button = dialog.findChild(GenericPredicate("Allow", roleName='push button'), retry=False, requireResult=False)
        if not allow_button:
            doDelay(0.5)
            continue
        if not allow_button.showing:
            # Scroll to confirmation button..
            sleep(0.5)
            pressKey('space')
            pressKey('space')
            pressKey('space')
            pressKey('space')
            pressKey('space')
            break
        else:
            doDelay(0.5)

    # Wait for button to become enabled
    for attempts in range(0, 50):
        if pyatspi.STATE_SENSITIVE in \
                allow_button.getState().getStates():
            break
        else:
            doDelay(1)

    allow_button.click()
    assert get_showing_node_name('Remove Account', gcc), \
        "Meta dialog is not showing"
    #assert wait_until(lambda x: x.showing, dialog.child(name='Remove Account')), \
    pressKey('Esc')
    #assert wait_until(lambda x: not x.showing, dialog), "Failed to close the dialog"


def add_windows_live_account(context, user, password):
    dialog = get_showing_node_name('Microsoft Account', context.app.instance)
    sleep(5)
    typeText(user)
    pressKey('Enter')
    doDelay(5)
    typeText(password)
    pressKey('Enter')
    doDelay(5)

    # Wait for GNOME icon to appear
    third_party_icon_pred = GenericPredicate(roleName='document web',
                                             name='Let this app access your info?')
    for attempts in range(0, 10):  # pylint: disable=W0612
        if dialog.findChild(third_party_icon_pred,
                            retry=False,
                            requireResult=False) is not None:
            break
        else:
            doDelay(5)

    doDelay(3)
    allow_button = dialog.child("Yes", roleName='push button')
    if not allow_button.showing:
        # Scroll to confirmation button
        scrolls = dialog.findChildren(
            GenericPredicate(roleName='scroll bar'))
        scrolls[-1].value = scrolls[-1].maxValue
        pressKey('space')
        pressKey('space')

    # Wait for button to become enabled
    for attempts in range(0, 10):
        if pyatspi.STATE_SENSITIVE in \
                allow_button.getState().getStates():
            break
        else:
            doDelay(1)
    allow_button.click()
    assert wait_until(lambda x: x.showing, dialog.child(name='Remove Account')), \
        "Meta dialog is not showing"
    sleep(1)
    pressKey('Esc')
    assert wait_until(lambda x: not x.showing, dialog), "Failed to close the dialog"


def add_facebook_account(context, user, password):
    dialog = context.app.instance.dialog('Facebook account')
    # Input credentials
    assert wait_until(lambda x: x.findChildren(
        GenericPredicate(roleName='entry')) != [],
        dialog, timeout=90), \
        "Facebook auth window didn't appear"
    entry = dialog.child(roleName='entry')
    if entry.text != user:
        entry.text = user
    dialog.child(roleName='password text').text = password
    doDelay(3)
    dialog.child(roleName='password text').grabFocus()
    pressKey('Enter')
    assert wait_until(lambda x: x.dead, dialog), \
        "Dialog was not closed"


def fill_in_credentials(context, cfg):
    user = cfg['id']
    pswd = cfg['password']
    acc_name = cfg['name']

    if acc_name in Google_accounts:
        add_google_account(context, user, pswd)

    if acc_name == 'Nextcloud':
        dialog = context.app.instance.child(name='Nextcloud Account')
        dialog.childLabelled('Server').text = cfg['server']
        dialog.childLabelled('Username').text = user
        dialog.childLabelled('Password').text = pswd
        dialog.button('Connect').click()
        assert wait_until(lambda x: x.showing, dialog.child(name='Remove Account')), \
            "Meta dialog is not showing"
        pressKey('Esc')
        assert wait_until(lambda x: not x.showing, dialog), "Failed to close the dialog"

    if 'Exchange' in acc_name:
        dialog = get_showing_node_name('Microsoft Exchange Account', context.app.instance, 'dialog')
        dialog.childLabelled("E-mail").text = 'NIX'
        dialog.childLabelled("E-mail").text = '%s' % cfg['email']
        dialog.childLabelled("Password").text = 'RedHat1!'
        get_showing_node_name('Custom', dialog).click()
        dialog.childLabelled("Server").text = cfg['server']
        get_showing_node_name('Connect', dialog).click()
        #get_showing_node_name('Error connecting to Microsoft Exchange server:\nThe signing certificate authority is not known.', dialog)
        #sleep(0.5)
        ignore = get_showing_node_name('Ignore', dialog)
        sleep(0.5)
        ignore.click()
        assert wait_until(lambda x: x.showing, dialog.child(name='Remove Account')), \
            "Meta dialog is not showing"

    if acc_name == 'Facebook':
        add_facebook_account(context, user, pswd)

    if acc_name == 'Microsoft Account':
        add_windows_live_account(context, user, pswd)

    if 'Kerberos' in acc_name:
        principal = '{}@{}'.format(cfg['user'], cfg['realm'])
        dialog = get_showing_node_name('{} Account'.format(cfg['provider']),
                                       context.app.instance, 'dialog')
        dialog.childLabelled("Principal").text = principal
        dialog.button('Connect').click()
        assert wait_until(lambda x: not x.showing, dialog), \
            "Failed to close the dialog"


@step('Add account "{acc_type}" via GOA')
def add_account_via_goa(context, acc_type):
    context.execute_steps('* Open online-accounts panel')
    # substitute Owncloud to Nextcloud, so we musn't replace actual step in project
    acc_type = 'Nextcloud' if acc_type == 'Owncloud' else acc_type

    account_cfg = context.app.get_account_configuration(acc_type, 'GOA')
    def get_account_string(acc_type):
        if acc_type in accounts:
            return '%s\n%s' % (account_cfg['provider'], account_cfg['id'])
        elif "Exchange" in acc_type:
            return '%s\n%s' % (account_cfg['provider'], account_cfg['email'])
        elif acc_type =='Nextcloud':
            return '%s\n%s@%s' % (account_cfg['provider'], account_cfg['id'], account_cfg['server'])
        elif "Kerberos" in acc_type:
            return '{}\n{}@{}'.format(account_cfg['provider'],
                                      account_cfg['user'], account_cfg['realm'])
        else:
            raise Exception("Cannot form account string for '%s' type" % acc_type)

    log.debug('acc_type = {}'.format(acc_type))

    account_string = get_account_string(acc_type)
    account_exists = context.app.instance.findChild(
        GenericPredicate(account_string),
        retry=False, requireResult=False)
    if not account_exists:
        if acc_type in Google_accounts:
            cleanup()
            context.app.instance.child("Google").click()
        if 'Exchange' in acc_type:
            context.app.instance.child('Microsoft Exchange').click()
        if acc_type == 'Windows Live' or acc_type == "Microsoft Account":
            cleanup()
            context.app.instance.child('Microsoft').click()
        if acc_type == 'Nextcloud':
            context.app.instance.child(name='Nextcloud').click()
        if 'Kerberos' in acc_type:
            acc_provider = account_cfg['provider']
            log.debug('acc_provider = {}'.format(acc_provider))
            acc_prvdr_but = context.app.instance.child(name=acc_provider).labellee
            log.debug('acc_prvdr_but = {}'.format(acc_prvdr_but))
            acc_prvdr_but[0].click()
        sleep(2)
        fill_in_credentials(context, account_cfg)
        assert get_showing_node_name(account_string, context.app.instance) is not None,\
            "GOA account was not added"


def cleanup():
    call('rm -rf /home/test/.cache/goa-1.0/', shell=True)
    call('rm -rf /home/test/.cache/gnome-control-center/', shell=True)
    call('rm -rf /home/test/.local/share/webkitgtk/', shell=True)


@step('Handle authentication window with password "{password}"')
def handle_authentication_window_with_custom_password(context, password):
    handle_authentication_window(context, password)


@step('Handle authentication window')
def handle_authentication_window(context, password='redhat'):
    # Get a list of applications
    app_names = []
    for attempt in range(0, 15):
        try:
            app_names = list(map(lambda x: x.name, root.applications()))
            break
        except GLib.GError:
            sleep(1)
            continue
    if 'gcr-prompter' in app_names:
        # Non gnome shell stuf
        passprompt = root.application('gcr-prompter')
        continue_button = passprompt.findChild(
            GenericPredicate(name='Continue'),
            retry=False, requireResult=False)
        if continue_button:
            passprompt.findChildren(GenericPredicate(roleName='password text'))[-1].grab_focus()
            sleep(1)
            typeText(password)
            # Don't save passwords to keyring
            keyCombo('<Tab>')
            # Click Continue
            keyCombo('<Tab>')
            keyCombo('<Tab>')
            keyCombo('<Enter>')
    elif 'gnome-shell' in app_names:
        shell = root.application('gnome-shell')
        if wait_until(lambda x: x.findChildren(
                lambda x: x.roleName == 'password text' and x.showing) != [], shell):
            st = shell.child('Add this password to your keyring')
            if not st.parent.parent.checked:
                st.click()
            pswrd = shell.child(roleName='password text')
            pswrd.click()
            typeText(password)
            keyCombo('<Enter>')
            wait_until(st.dead)
            sleep(3)
